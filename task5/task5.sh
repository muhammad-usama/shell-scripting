#!/bin/bash
data=$(egrep -ro 'run-time-hours [0-9]*[.][0-9]*$' ./logs) # r (recursive), o (only show matched part)
count=1
logfile=''
for d in $data; do
    if [ $((count%2)) == 0 ]; then
        d=$(echo "$d" | egrep -o '[0-9]*[.][0-9]{2}')
        if [ $(echo "$d < 9.5" | bc) == 1 ]; then 
            echo 'Filename: ' $logfile ' Time: ' $d
        fi
    else
       logfile=$(echo $d | egrep -o '[[:alpha:]]*_recon-all.log' )
    fi
    count=$((count+1))
done

# Alternate Solution
#data=$(egrep -Hro 'run-time-hours ([0-8][.][0-9]{2}|[9][.][5][0]|[9][.][0-4][0-9])' ./logs)
#for f in $data; do
#    if [ $((count%2)) == 0 ]; then
#        echo $f      
#    fi
#done