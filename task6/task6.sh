#!/bin/bash
numbers=$(grep -o '\b[[:digit:]]*' raw_data_for_stats_calculation.txt)
echo "$numbers" > numbers.txt
numbers=$(echo "$numbers" | sort -n)
echo "$numbers" > sorted_numbers.txt

sum=0
min=1000
max=0
avg=0
for num in $numbers;do
    sum=$((sum+num))
    if [ $num -lt $min ]; then
        min=$num
    fi
    if [ $num -gt $max ]; then
        max=$num
    fi
done
#mean=$(printf %.2f $(bc -l <<< $sum/200))
mean=$(echo "scale=2; $sum / 200" | bc )
echo -e 'Sum:  ' $sum '\nMin:  ' $min '\nMax:  ' $max '\nMean: ' $mean
#echo $(ministat -sn sorted_numbers.txt)
