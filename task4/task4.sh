#!/bin/bash
basePath="/home/"
if [ ! -d "$basePath""Backup" ]; then
	sudo mkdir "$basePath"Backup
	echo 'folder created'
else
	files=$(find $1 | grep -o '[[:alnum:]]*.c$')
	for f in $files; do 
		if [ ! -f "$basePath""Backup/$f" ]; then
			sudo cp $1$f "$basePath""Backup"
			echo "The File $f had no previous copy and is now backed up"
		else 
			if ! cmp -s "$basePath""Backup/$f" "$1$f" ; then # s (supress output)
				sudo cp $1$f "$basePath""Backup"
				echo "The file $f has been updated"
			fi
		fi
	done
fi
